package com.db.benchmarks.enumeration;

/**
 * Numerazione per codici di errore
 */
public enum ErrorCode {
    AUTONONTROVATA(100),ERROREDATABASE(200),ERROREINSERIMENTO(300),DATIMANCANTI(400);

    ErrorCode(int i) {

    }
}
