package com.db.benchmarks.exception;

import com.db.benchmarks.enumeration.ErrorCode;
import com.db.benchmarks.model.Auto;
import lombok.Data;

/**
 * Eccezzione personalizzata
 */
@Data
public class AutoException extends Exception{
    private String message;
    private ErrorCode codiceErrore;

    public AutoException (String errorCode){
        this.codiceErrore = ErrorCode.valueOf(errorCode);
    }
}
