package com.db.benchmarks.repository;

import com.db.benchmarks.model.TimeResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Repository per l'interazione col db tramite Hibernate e Jpa
 * Query native per ogni possibile richiesta
 */
@Repository
public interface TimeResponseRepository extends JpaRepository<TimeResponse, Long> {


    @Query(value = "select time from time_response where nome_metodo = '/selectByIdPS' order by time desc limit 1",nativeQuery = true)
    Long getMaxSelectPs();
    @Query(value = "select time from time_response where nome_metodo = '/selectById' order by time desc limit 1",nativeQuery = true)
    Long getMaxSelect();
    @Query(value = "select time from time_response where nome_metodo = '/selectByIdPS' order by time asc limit 1",nativeQuery = true)
    Long getMinSelectPs();
    @Query(value = "select time from time_response where nome_metodo = '/selectById' order by time asc limit 1",nativeQuery = true)
    Long getMinSelect();
    @Query(value="select AVG(time) from time_response where nome_metodo = '/selectById'",nativeQuery = true)
    Long getAvgSelect();
    @Query(value="select AVG(time) from time_response where nome_metodo = '/selectByIdPs'",nativeQuery = true)
    Long getAvgSelectPs();
    @Query(value = "select time from time_response where nome_metodo = '/insertPs' order by time desc limit 1",nativeQuery = true)
    Long getMaxInsertPs();
    @Query(value = "select time from time_response where nome_metodo = '/insert' order by time desc limit 1",nativeQuery = true)
    Long getMaxInsert();
    @Query(value = "select time from time_response where nome_metodo = '/insertPs' order by time asc limit 1",nativeQuery = true)
    Long getMinInsertPs();
    @Query(value = "select time from time_response where nome_metodo = '/insert' order by time asc limit 1",nativeQuery = true)
    Long getMinInsert();
    @Query(value="select AVG(time) from time_response where nome_metodo = '/insert'",nativeQuery = true)
    Long getAvgInsert();
    @Query(value="select AVG(time) from time_response where nome_metodo = '/insertPs'",nativeQuery = true)
    Long getAvgInsertPs();


}
