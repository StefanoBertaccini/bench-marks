package com.db.benchmarks.repository;

import com.db.benchmarks.model.Auto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Respository per l'interazione col db tramite hibernate e jpa
 */
@Repository
public interface AutoRepository extends JpaRepository<Auto, Long> {

}
