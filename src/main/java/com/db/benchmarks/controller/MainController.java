package com.db.benchmarks.controller;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.model.TimeResponse;
import com.db.benchmarks.service.AutoService;
import com.db.benchmarks.exception.AutoException;
import com.db.benchmarks.model.Auto;
import com.db.benchmarks.service.AutoServiceJdbc;
import com.db.benchmarks.service.TimeResponseService;
import com.db.benchmarks.util.Timeresponsebuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller per le azioni principali di insert e select nei due metodi spiegati nel ReadMe
 */
@RestController
public class MainController {
    @Autowired
    private AutoService autoService;
    @Autowired
    private AutoServiceJdbc autoServiceJdbc;
    @Autowired
    private ServletContext servletContext;
    @Autowired
    private Timeresponsebuilder timeresponsebuilder;
    @Autowired
    private TimeResponseService timeResponseService;

    StopWatch stopWatch = new StopWatch();

    /**
     * Metdodo per l'insert comune tramite Jpa e il suo repository, con uno StopWatch per i tempi di prestazione
     * @param autoDto
     * @param httpServletRequest
     * @return
     */
    @PostMapping(value = "/db/benchmarks/insert")
    public Auto insertAuto(@RequestBody @Valid AutoDto autoDto, HttpServletRequest httpServletRequest){
        //Avvio lo stopWatch
        stopWatch.start();
        //Richiamo il service e richiamo il metodo per l'insert
        Auto auto = autoService.insertAuto(autoDto);
        //Blocco lo stopWatch
        stopWatch.stop();
        //Inserisco tramite l'altro service il tempo e il metodo che lo richiama
        timeResponseService.insert(timeresponsebuilder.getTime(stopWatch.getTotalTimeMillis(),httpServletRequest.getRequestURI().substring(httpServletRequest.getRequestURI().lastIndexOf("/"))));
        return auto;
    }

    /**
     * Metodo per la select con Primarykey tramite Jpa
     * @param id
     * @param httpServletRequest
     * @return
     * @throws AutoException
     */
    @GetMapping(value = "/db/benchmarks/selectByid")
    public Auto selectAuto(@RequestParam(value = "id") Long id, HttpServletRequest httpServletRequest) throws AutoException {
        stopWatch.start();
        if(autoService.selectAutoById(id).isPresent()){
            stopWatch.stop();
            timeResponseService.insert(timeresponsebuilder.getTime(stopWatch.getTotalTimeMillis(),httpServletRequest.getRequestURI().substring(httpServletRequest.getRequestURI().lastIndexOf("/"))));
            return autoService.selectAutoById(id).get();
        }
        else{
            throw new AutoException("ERROREDATABASE");
        }

    }

    /**
     * Metodo per l'insert tramite il service Jdbc
     * @param autoDto
     * @param httpServletRequest
     * @return
     * @throws AutoException
     */
    @PostMapping(value = "/db/benchmarks/insertPS")
    public int insertAutoPS(@RequestBody @Valid AutoDto autoDto, HttpServletRequest httpServletRequest) throws AutoException {
        stopWatch.start();
        int result = autoServiceJdbc.insertAuto(autoDto);
        stopWatch.stop();
        if(result ==1) {
            timeResponseService.insert(timeresponsebuilder.getTime(stopWatch.getTotalTimeMillis(), httpServletRequest.getRequestURI().substring(httpServletRequest.getRequestURI().lastIndexOf("/"))));
        }
        return result;
    }

    /**
     * Metodo per la select tramite service Jdbc
     * @param id
     * @param httpServletRequest
     * @return
     * @throws AutoException
     */
    @GetMapping(value = "/db/benchmarks/selectByidPS")
    public Auto selectAutoPS(@RequestParam(value = "id") Long id, HttpServletRequest httpServletRequest) throws AutoException {
        stopWatch.start();
        if(autoServiceJdbc.selectAutoById(id).isPresent()){
            Auto result = autoService.selectAutoById(id).get();
            stopWatch.stop();
            timeResponseService.insert(timeresponsebuilder.getTime(stopWatch.getTotalTimeMillis(),httpServletRequest.getRequestURI().substring(httpServletRequest.getRequestURI().lastIndexOf("/"))));
            return result;
        }
        else{
            throw new AutoException("ERROREDATABASE");
        }

    }


}
