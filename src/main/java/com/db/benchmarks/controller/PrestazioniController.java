package com.db.benchmarks.controller;


import com.db.benchmarks.service.TimeResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller con tutti gli endpoint per MAX MIN e AVG delle prestazioni dei vari metodi di insert e select
 * Nomi differenti in base alla categoria di metodo a cui fanno riferimento es.*Ps per tutti i metodi
 * che fanno affidamento al prepared statement
 */
@Controller
public class PrestazioniController {
    @Autowired
    private TimeResponseService timeResponseService;

    @GetMapping(value ="/db/benchmarks/getMaxSelect")
    public Long getMaxSelect(){

        return timeResponseService.getMaxSelect();
    }
    @GetMapping(value ="/db/benchmarks/getMinSelect")
    public Long getMinSelect(){

        return timeResponseService.getMinSelect();
    }
    @GetMapping(value ="/db/benchmarks/getAvgSelect")
    public Long getAvgSelect(){
        return timeResponseService.getAvgSelect();
    }
    @GetMapping(value ="/db/benchmarks/getMaxSelectPs")
    public Long getMaxSelectPs(){
        return timeResponseService.getMaxSelectPs();
    }
    @GetMapping(value ="/db/benchmarks/getMinSelectPs")
    public Long getMinSelectPs(){

        return timeResponseService.getMinSelectPs();
    }
    @GetMapping(value ="/db/benchmarks/getAvgSelectPs")
    public Long getAvgSelectPs(){
        return timeResponseService.getAvgSelectPs();
    }
    @GetMapping(value ="/db/benchmarks/getMaxInsert")
    public Long getMaxInsert(){
        return timeResponseService.getMaxInsert();
    }
    @GetMapping(value ="/db/benchmarks/getMinInsert")
    public Long getMinInsert(){

        return timeResponseService.getMinInsert();
    }
    @GetMapping(value ="/db/benchmarks/getAvgInsert")
    public Long getAvgInsert(){
        return timeResponseService.getAvgInsert();
    }
    @GetMapping(value ="/db/benchmarks/getMaxInsertPs")
    public Long getMaxInsertPs(){
        return timeResponseService.getMaxInsertPs();
    }
    @GetMapping(value ="/db/benchmarks/getMinInsertPs")
    public Long getMinInsertPs(){

        return timeResponseService.getMinInsertPs();
    }
    @GetMapping(value ="/db/benchmarks/getAvgInsertPs")
    public Long getAvgInsertPs(){
        return timeResponseService.getAvgInsertPs();
    }

}
