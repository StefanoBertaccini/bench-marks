package com.db.benchmarks.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

/**
 * Classe che modella l'entity dei tempi di risposta dell'applicazione
 * Lombok per getter setter e costruttori
 * Id autogerato
 */
@Data
@Entity
public class TimeResponse {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private long time;

    private String nomeMetodo;

    public TimeResponse(long time,String nomeMetodo){
        this.time = time;
        this.nomeMetodo = nomeMetodo;
    }

}
