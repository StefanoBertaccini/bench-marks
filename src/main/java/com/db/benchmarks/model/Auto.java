package com.db.benchmarks.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Entity hibernate per la creazione della tabella sul db
 * Con id Autogenerato Identity
 * Lombock per costruttori getter e setter
 */
@Data
@Entity
@Getter
@Setter
public class Auto {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String modello;

    private String marca;

    private int porte;

    private int cilindrata;

    private int cavalli;

}
