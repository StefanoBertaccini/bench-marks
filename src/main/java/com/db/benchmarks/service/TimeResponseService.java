package com.db.benchmarks.service;

import com.db.benchmarks.model.TimeResponse;

/**
 * Interfaccia del service con tutti i metodi necessari per la richiesta dei tempi di processo
 */
public interface TimeResponseService {
    TimeResponse insert(TimeResponse timeResponse);
    Long getMaxSelect();
    Long getMinSelect();
    Long getAvgSelect();
    Long getMaxSelectPs();
    Long getMinSelectPs();
    Long getAvgSelectPs();
    Long getMaxInsert();
    Long getMinInsert();
    Long getAvgInsert();
    Long getMaxInsertPs();
    Long getMinInsertPs();
    Long getAvgInsertPs();
}
