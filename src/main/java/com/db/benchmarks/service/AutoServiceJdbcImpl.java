package com.db.benchmarks.service;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.exception.AutoException;
import com.db.benchmarks.model.Auto;
import org.hibernate.engine.jdbc.batch.spi.Batch;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service per l'interazione col db tramite jdbc
 */
@Service
public class AutoServiceJdbcImpl implements AutoServiceJdbc {

    private int batchCount;
    private List<String> listaSQlBatch = new ArrayList<>();
    private Connection connection = null;
    private PreparedStatement preparedStatement =null;

    /**
     * Metodo per l'aggiunta di un bean al db tramite JDBC
     * @param autoDto
     * @return
     * @throws AutoException
     */
    @Override
    public int insertAuto(AutoDto autoDto) throws AutoException {
        try {
            //Controllo che la connessione non sia nulla, se no la inizializzo con l'url per il mio database Postgre
            if(connection==null) {
                connection = DriverManager.getConnection("jdbc:postgresql://localhost:5433/Benchmarks?user=postgres&password=kidberta");
                //imposto la connessione con autocommit a false
                connection.setAutoCommit(false);
            }
            //Controllo che il preparedStatement non sia nullo se no l'ho inizializzo con la insert la mia entity
            if(preparedStatement==null){
                preparedStatement =connection.prepareStatement("INSERT INTO auto (marca,modello,porte,cavalli,cilindrata) VALUES (?,?,?,?,?);");
            }
            //Setto i valori da attribuire per l'insert tramite i set del preparedStatement
            preparedStatement.setString(1,autoDto.getMarca());
            preparedStatement.setString(2,autoDto.getModello());
            preparedStatement.setInt(3,autoDto.getPorte());
            preparedStatement.setInt(4,autoDto.getCavalli());
            preparedStatement.setInt(5,autoDto.getCilindrata());
            //Controllo che il mio contatore di bacth non sia superiore al limite prestabilito e aggiungo la query
            //Di insert al batch del preparedStatement e ritorno 0
            if(batchCount<1){
                    preparedStatement.addBatch();
                    batchCount++;
                    return 0;
                }else{
                //Se ho raggiunto il numero di query stabilito aggiungo comunque la query al batch
                //E poi lo eseguo per aggiungere le x query
                    preparedStatement.addBatch();
                    preparedStatement.clearParameters();
                    preparedStatement.executeBatch();
                    //Faccio la commit della connessione, avendola impostato l'auto commit a false
                    //E chiudo il PreparedStatement e la connessione e ritorno 1
                    connection.commit();
                    preparedStatement.close();
                    connection.close();
                    return 1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
                throw new AutoException("ERROREDATABASE");
        }

    }

    @Override
    public Auto deleteAuto(Long idAuto) {
        return null;
    }

    /**
     * Metodo per la select di una entity tramite PK sfruttando JDBC
     * @param idAuto
     * @return
     * @throws AutoException
     */
    @Override
    public Optional<Auto> selectAutoById(Long idAuto) throws AutoException {
        //Inizializzo un resultset e il mio valore di ritorno come Optional empty pernon ricaedere in un null pointer
        ResultSet resultSet;
        Optional<Auto> result= Optional.empty();
        //Ho un try with resource dove inizializzo la copnnessione
        try(Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5433/Benchmarks?user=postgres&password=kidberta")) {
            //Inizializzo pure il preparedStatement con la query necessaria per interrogare il db
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM auto where id = ?");
            //Setto il parametro necessario
            preparedStatement.setLong(1,idAuto);
            //Controllo che la query fatta non sia nulla , nel caso restituisco l'optional vuoto
            if(preparedStatement.executeQuery().wasNull()){
                return result;
            }else{
                //Se no eseguo la query e creo il bean tramite il resultset e lo restitutisco dentro all'optional
                resultSet=preparedStatement.executeQuery();

                Auto auto = new Auto();
                while(resultSet.next()) {
                    auto.setModello(resultSet.getString(5));
                    auto.setMarca(resultSet.getString(4));
                    auto.setCavalli(resultSet.getInt(2));
                    auto.setCilindrata(resultSet.getInt(3));
                    auto.setPorte(resultSet.getInt(6));
                    auto.setId(idAuto);
                    result = Optional.of(auto);
                    preparedStatement.close();
                    resultSet.close();
                    return result;
                }
                return result;
            }

        } catch (SQLException throwables) {
            throw new AutoException("ERROREDATABASE");
        }
    }

    @Override
    public List<Auto> selectAll() {
        return null;
    }
}
