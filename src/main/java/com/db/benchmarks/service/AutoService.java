package com.db.benchmarks.service;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.model.Auto;

import java.util.List;
import java.util.Optional;

/**
 * Interfaccia per il service dell'entity principale, che si basa su Jpa
 */
public interface AutoService {
    Auto insertAuto(AutoDto autoDto);
    Auto deleteAuto(Long idAuto);
    Optional<Auto> selectAutoById(Long idAuto);
    List<Auto> selectAll();
}
