package com.db.benchmarks.service;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.exception.AutoException;
import com.db.benchmarks.model.Auto;

import java.util.List;
import java.util.Optional;

/**
 * interfaccia del service che si abserà su JDBC
 */
public interface AutoServiceJdbc {
    int insertAuto(AutoDto autoDto) throws AutoException;
    Auto deleteAuto(Long idAuto);
    Optional<Auto> selectAutoById(Long idAuto) throws AutoException;
    List<Auto> selectAll();
}
