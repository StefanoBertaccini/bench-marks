package com.db.benchmarks.service;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.model.Auto;
import com.db.benchmarks.repository.AutoRepository;
import com.db.benchmarks.util.AutoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Implementazione dell'interfaccia service per l'interazione col db tramite Jpa
 */
@Service
public class AutoServiceImpl implements AutoService {
    @Autowired
    private AutoRepository autoRepository;
    @Autowired
    private AutoMapper autoMapper;

    /**
     * Metodo per l'inserimento di un auto tramite bean
     * @param autoDto
     * @return
     */
    @Override
    public Auto insertAuto(AutoDto autoDto) {
        Auto result = autoMapper.getAuto(autoDto);
        autoRepository.save(result);
        return result;
    }

    /**
     * Metodo per l'eliminazione di un auto tramite bean ricavato
     * dalla primary key
     * @param idAuto
     * @return
     */
    @Override
    public Auto deleteAuto(Long idAuto) {
        Auto autoDeleted = autoRepository.getById(idAuto);
        autoRepository.delete(autoDeleted);
        return autoDeleted;
    }

    /**
     * Metodo per la select di una entity tramite PK
     * @param idAuto
     * @return
     */
    @Override
    public Optional<Auto> selectAutoById(Long idAuto) {

        return autoRepository.findById(idAuto);
    }

    @Override
    public List<Auto> selectAll() {

        return autoRepository.findAll();
    }
}
