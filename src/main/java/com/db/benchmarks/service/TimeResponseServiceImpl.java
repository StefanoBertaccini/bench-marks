package com.db.benchmarks.service;

import com.db.benchmarks.model.TimeResponse;
import com.db.benchmarks.repository.TimeResponseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementazione del service con tutti i metodi per gli endpoint sul controller
 */
@Service
public class TimeResponseServiceImpl implements TimeResponseService{
    @Autowired
    TimeResponseRepository timeResponseRepository;

    /**
     * Metodo per l'insert dei tempi di prestazione all'interno del Db
     * @param timeResponse
     * @return
     */
    @Override
    public TimeResponse insert(TimeResponse timeResponse) {
        timeResponseRepository.save(timeResponse);
        return timeResponse;
    }

    @Override
    public Long getMaxSelect() {
        return timeResponseRepository.getMaxSelect();
    }

    @Override
    public Long getMinSelect() {
        return timeResponseRepository.getMinSelect();
    }

    @Override
    public Long getAvgSelect() {
        return timeResponseRepository.getAvgSelect();
    }
    @Override
    public Long getMaxSelectPs() {
        return timeResponseRepository.getMaxSelectPs();
    }

    @Override
    public Long getMinSelectPs() {
        return timeResponseRepository.getMinSelectPs();
    }

    @Override
    public Long getAvgSelectPs() {
        return timeResponseRepository.getAvgSelectPs();
    }

    @Override
    public Long getMaxInsert() {
        return timeResponseRepository.getMaxInsert();
    }

    @Override
    public Long getMinInsert() {
        return timeResponseRepository.getMinInsert();
    }

    @Override
    public Long getAvgInsert() {
        return timeResponseRepository.getAvgInsert();
    }

    @Override
    public Long getMaxInsertPs() {
        return timeResponseRepository.getMaxInsertPs();
    }

    @Override
    public Long getMinInsertPs() {
        return timeResponseRepository.getMinInsertPs();
    }

    @Override
    public Long getAvgInsertPs() {
        return timeResponseRepository.getAvgInsertPs();
    }

}
