package com.db.benchmarks.util;

import com.db.benchmarks.model.TimeResponse;
import org.springframework.stereotype.Component;

/**
 * Classe di utilità per la creazione della timeResponse
 */
@Component
public class Timeresponsebuilder {

    public TimeResponse getTime(long time, String nomeMetodo){
        TimeResponse timeResponse = new TimeResponse(time,nomeMetodo);
        return timeResponse;
    }
}
