package com.db.benchmarks.util;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.model.Auto;
import org.springframework.stereotype.Component;

import javax.annotation.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-11-08T15:25:26+0100",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 1.8.0_301 (Oracle Corporation)"
)
@Component
public class AutoMapperImpl implements AutoMapper {

    @Override
    public Auto getAuto(AutoDto dto) {
        if ( dto == null ) {
            return null;
        }

        Auto auto = new Auto();

        auto.setModello( dto.getModello() );
        auto.setMarca( dto.getMarca() );
        auto.setPorte( dto.getPorte() );
        auto.setCilindrata( dto.getCilindrata() );
        auto.setCavalli( dto.getCavalli() );

        return auto;
    }
}
