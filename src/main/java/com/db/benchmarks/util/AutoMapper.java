package com.db.benchmarks.util;

import com.db.benchmarks.dto.AutoDto;
import com.db.benchmarks.model.Auto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * interfaccia del mapper da DTO a Entity
 */
@Mapper
public interface AutoMapper {
    AutoMapper INSTANCE = Mappers.getMapper(AutoMapper.class);

    Auto getAuto(AutoDto dto);
}
