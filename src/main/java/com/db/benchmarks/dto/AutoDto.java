package com.db.benchmarks.dto;
import lombok.Data;


/**
 * DTO dell entity del db
 */
@Data
public class AutoDto {

    private String modello;

    private String marca;

    private int porte;

    private int cilindrata;

    private int cavalli;
}
