package com.db.benchmarks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//Annotazione per avviare swagger con il lancio dell'applicazione
@SpringBootApplication
@EnableSwagger2
public class BenchmarksApplication {

	public static void main(String[] args) {
		SpringApplication.run(BenchmarksApplication.class, args);
	}

}
